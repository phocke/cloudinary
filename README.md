# To run the app:

Once you've successfully bootstrapped both `cloudinary-api` as well as `cloudinary-frontend` you can run both applications using foreman. One command will start both of your apps as well as redis instance. All of the logs will be attached to one console (you can still use `tail -f` in your respective apps)

To use foreman:

```
gem install foreman
foreman start (in the project root directory)
```

# Some background info on the project and how it went

First of all - I've selected the OMDB integration project as the one to work on (you can find description in a PDF attached to this repo)

Regarding the back end - it's a fairly simple app bootstrapped using `rails new --api`. It uses redis as a caching mechanism as it's a perfect solution for key-value storage. Sqlite is added as a dependency, but it's not needed. I didn't want to spend time trying to find out where that dependency is introduced.

Regarding the front end - the number one improvement worth addressing would be state management. I've opted for react-hooks as it seemed simple enough initially. At this stage tough I would opt for adding some reducers as the code gets sufficiently complex. I've tried not to introduce too many dependencies to give you a better idea about my fundamental knoledge of react and JS in general.

#Couple things worth addressing in case it would become a production app:

- API access token is hard-coded in the API handling code. Given more time I would setup `.env` files appropriately to make it conform to 12-factor apps guidelines.
- I didn't write any tests as setting up a testing frameworks would take another hour or so per app. I would use `rspec` with `VCR` or `moca` for the back end. I would opt for `jest` to test the react components. Regarding e2e tests I would discuss the approach with the team. Obvious solution seems to be selenium, but I consider it to be slightly outdated and upkeep of the test suite becomes cumbersome fairly quickly. 
- I would also add the a single command build to the root of the project that would link to respective build scripts in subrepos. 
- Lackluster documentation - I would start by describing the JSON API using swagger or similar tool, to document the available endpoints and the shape of expected responses.
 
My thoughts regarding the task - it was a fun little project to work on :). I've added keybord navigation to it, as I try to avoid using mouse since keyboard is simply a quicker way to get around for power users.

I don't particularly like the OMDB API as it doesn't conform to all the widely accepted modern standards - ie. in case there's too many results to be returned it it just returns an error rather than paginated results. There's more problems with it obviously, but that's beyond the scope of this document.
